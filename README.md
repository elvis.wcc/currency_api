# currency_api

This API is a simple abstraction layer for dealing with insertion and retrieval to a PostgreSQL database.
Migrations are made with flask, SQLAlchemy and alembic.
Database access is made with psycopg2.
Insertions are made with POST requests, and retrieving of data by GET requests.


## Setting up

### Config

Modify 'config.sh' to configure paths and database parameters


### Migrations

Execute 'migration.sh' to make the migrations


### Executing server

Configure the host and port in 'config.sh' and execute 'run_server.sh' to set up the server


## Using the API

### Inserting data

The POST requests are made to the path 'http://[HOST]:[PORT]/insert?update=[True or False]',
and must have a valid JSON payload according to the schema supplied in 
'src/schemas/schema.json'. An example is shown in 'src/schemas/sample.json'.

Parameter 'update' specifies if values should be overwritten if a record with same provider, 
pair and time aready exists. If not specified in the url, it is assumed to be False.

Payload validations are performed and errors are returned according to the problem.


### Retrieving data

The retrieval of data is made by GET requests to the path 
'http://[HOST]:[PORT]/retrieve/[PROVIDER]/[PAIR]?ini_date=[INITIAL DATE]&fin_date=[FINAL DATE]'
