export CURR_API_PATH=/home/elvis/prog_sandbox/python/currency_api
export PYTHONPATH=$CURR_API_PATH
export PATH=$PATH:$CURR_API_PATH
export DATABASE_URL='localhost:5432'
export DATABASE_NAME='currencies'
export DATABASE_USER='postgres'
export DATABASE_PASS='foo'

export API_HOST=127.0.0.1
export API_PORT=5001