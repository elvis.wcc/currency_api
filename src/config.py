import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    USER = os.environ['DATABASE_USER']
    PASS = os.environ['DATABASE_PASS']
    URL = os.environ['DATABASE_URL']
    NAME = os.environ['DATABASE_NAME']
    SQLALCHEMY_DATABASE_URI = "postgres://{}:{}@{}/{}".format(USER, PASS, URL, NAME)


class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
