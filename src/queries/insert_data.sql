INSERT INTO public.exchange_data (provider, pair, date, price, quantity)
VALUES
('{provider}', '{pair}', '{date}', {price}, {quantity})
ON CONFLICT (provider, pair, date) DO NOTHING;
