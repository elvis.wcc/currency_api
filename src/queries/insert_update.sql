INSERT INTO public.exchange_data (provider, pair, date, price, quantity)
VALUES
('{provider}', '{pair}', '{date}', {price}, {quantity})
ON CONFLICT (provider, pair, date) DO
     UPDATE
     SET
      price = {price},
      quantity  = {quantity}
     WHERE 
     public.exchange_data.provider = '{provider}' AND 
     public.exchange_data.pair = '{pair}' AND
     public.exchange_data.date = '{date}';
