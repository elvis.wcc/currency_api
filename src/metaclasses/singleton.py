# -*- coding: utf-8 -*-
# !/usr/bin/env python3

"""Singleton
Módulo que define o tipo Singleton para ser usado como metaclass
em outras classes do projeto
"""


class Singleton(type):
    """
    Singleton é o tipo a ser usado como metaclass de um objeto
    que se deseja ter apenas uma instância presenta durante a execução
    da aplicação
    """

    def __init__(cls, name, bases, attrs, **kwargs):
        super().__init__(name, bases, attrs)
        cls._instance = None

    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__call__(*args, **kwargs)
        return cls._instance
