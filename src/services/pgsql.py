import os
import time
import sys

import psycopg2.pool as pool

sys.path.extend(os.environ['CURR_API_PATH'])

from src.metaclasses.singleton import Singleton

USER = os.environ['DATABASE_USER']
PASS = os.environ['DATABASE_PASS']
URL = os.environ['DATABASE_URL'].split(':')[0]
NAME = os.environ['DATABASE_NAME']
CNN_STR = "dbname={} user={} host={} password={}".format(NAME, USER, URL, PASS)
MAX_RETRIES = 10
MINCONN = 1
MAXCONN = 20


class TooManyRetries(Exception):
    """
    Auxiliary class for pgsql exceptions
    """
    def __init__(self, message):
        super(TooManyRetries, self).__init__(message)


class PgSQLService(metaclass=Singleton):
    """
    psycopg2 wrapper class for dealing with queries
    """
    def __init__(self):
        self.pool = None
        self.path_to_queries = None

    def _open_pool(self, connection_string):
        if self.pool is None:
            self.pool = pool.ThreadedConnectionPool(
                MINCONN, MAXCONN, dsn=connection_string)

    def execute_query(self, query, retry=0, should_format=True, get_rows=False):
        """
        Method for executing prepared queries with formatting options
        :param str query: Prepared query
        :param int retry: Retry counter (reset to 0 at each execution)
        :param bool should_format: Whether or not to format response
        :param bool get_rows: Wheter or not to get number of affected rows
        :return: Query answer based on specified optins
        """
        if self.pool is None:
            self._open_pool(CNN_STR)

        try:
            conn = self.pool.getconn()

            cur = conn.cursor()
            conn.autocommit = True

            cur.execute(query)

            if should_format:
                result = cur.fetchall()
                cols = [c.name for c in cur.description]
                data = {c: [r[n] for r in result] for n, c in enumerate(cols)}
            elif get_rows:
                data = cur.rowcount
            else:
                data = None
            self.pool.putconn(conn)
            cur.close()

        except pool.PoolError as e:
            if retry > MAX_RETRIES:
                raise TooManyRetries("Too many retries: {}".format(e))
            time.sleep(1)
            return self.execute_query(query, retry + 1)
        except Exception as e:
            raise Exception("Error: Check query \n{}".format(e))
        return data

    @staticmethod
    def build_query(query_file, *args, **kwargs):
        """
        Auxiliary method for building queries based on files stored on src/queries
        :param str query_file: Name of .sql file
        :param args: Additional args (not used)
        :param kwargs: Parameters passed for query formatting
        :return: String containing query
        """
        with open(os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', "queries")),
                  query_file), "r", encoding="utf-8") as query:
            sql = query.read()
        sql = sql.format(*args, **kwargs)
        return sql
