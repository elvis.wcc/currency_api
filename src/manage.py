import os

from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from app import app, db

"""
This is the main server manager, where the main app is imported to be used under flask_script
"""

app.config.from_object("config.DevelopmentConfig")

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)


if __name__ == '__main__':
    manager.run()
