from app import db


class Exchange(db.Model):
    """
    Basic model containing the default table columns for the currencies API
    """

    __tablename__ = 'exchange_data'

    provider = db.Column(db.String(), primary_key=True)
    date = db.Column(db.DateTime(), primary_key=True)
    pair = db.Column(db.String(), primary_key=True)
    price = db.Column(db.Float())
    quantity = db.Column(db.Float())

    def __init__(self, provider, date, pair, price, quantity):
        self.provider = provider
        self.date = date
        self.pair = pair
        self.price = price
        self.quantity = quantity

    def __repr__(self):
        return '<source {}, date {}, pair {}>'.format(self.provider, self.date, self.pair)
