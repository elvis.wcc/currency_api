import datetime
import json
import os
import sys

from flask import Flask, request, jsonify
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from jsonschema import validate

sys.path.extend(os.environ['CURR_API_PATH'])

from src.services.pgsql import PgSQLService

app = Flask(__name__)
CORS(app)
app.config.from_object("config.DevelopmentConfig")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
pgsql = PgSQLService()

from models import Exchange


@app.route('/retrieve/<provider>/<pair>', methods=['GET'])
def retrieve(provider, pair):
    """
    Receives a request with provider, currency pair and date range to retrieve the
    currency exchange data (price, quantity) from the database, returnning a JSON answer.
    :param provider: Data provider
    :param pair: Currency pair
    :return: Json answer with retrieved data
    """
    if 'ini_date' and 'fin_date' in request.args:
        initi_date = request.args['ini_date']
        final_date = request.args['fin_date']

        retrieve_query = pgsql.build_query('retrieve_data.sql', provider=provider, pair=pair,
                                           initi=initi_date, final=final_date)
        result = pgsql.execute_query(query=retrieve_query, should_format=True)

        if len(result) > 0:
            return jsonify(result), 200
        else:
            return jsonify({'error': 'no data available for the requested specification'}), 205
    else:
        return jsonify({'error': "Missing required arguments",
                        'accepted_format': "'ini_date' and 'fin_date': YYYY-mm-ddTHH:MM:SS"}), 400


@app.route('/insert', methods=['GET', 'POST'])
def insert():
    """
    Receives currency data in JSON format to be inserted in the database under the
    specified provider and pair.
    Accepts a parameter 'update' specifying whether or not to overwrite existing data.
    :return:
    """
    if 'update' in request.args:
        query = 'insert_update.sql'
    else:
        query = 'insert_data.sql'

    if hasattr(request, 'json'):
        jdata = request.json
    else:
        return jsonify({'error': 'no JSON data was provided'}), 400

    with open(os.path.join(os.environ['CURR_API_PATH'], 'src/schemas/schema.json')) as ij:
        schema = json.load(ij)
    try:
        validate(jdata, schema)
    except Exception as e:
        return jsonify({'error': 'no valid JSON data was provided',
                        'details': str(e)}), 400

    try:
        dates = [datetime.datetime.strptime(d, '%Y-%m-%dT%H:%M:%S') for d in jdata['date']]
    except Exception:
        return jsonify({'error': 'invalid date encountered in provided data'}), 400

    lens = map(len, jdata.values())
    if len(set(lens)) != 1:
        return jsonify({'error': 'provided lists have different lengths'}), 400

    qrblock = ''
    for n, p in enumerate(jdata['provider']):
        qrblock += pgsql.build_query(query, provider=jdata['provider'][n], pair=jdata['pair'][n],
                                     date=jdata['date'][n], price=jdata['price'][n],
                                     quantity=jdata['quantity'][n])
    if len(qrblock) > 0:
        rows = pgsql.execute_query(qrblock, should_format=False, get_rows=True)
    else:
        return jsonify({'error': 'no valid JSON data was provided'}), 400

    return jsonify({'status': 'success',
                    'n_rows': len(jdata['date'])}), 200


if __name__ == '__main__':
    app.run()
